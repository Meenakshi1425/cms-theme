class AddBodyToHeader < ActiveRecord::Migration[5.2]
  def change
    add_column :headers, :body, :string
  end
end
