class RemoveHeaderToWidget < ActiveRecord::Migration[5.2]
  def change
    remove_column :widgets, :header, :string
  end
end
