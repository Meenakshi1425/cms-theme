class CreateMenus < ActiveRecord::Migration[5.2]
  def change
    create_table :menus do |t|
      t.string :name
      t.string :type
      t.string :label
      t.string :values
      t.string :redirect_url

      t.timestamps
    end
  end
end
