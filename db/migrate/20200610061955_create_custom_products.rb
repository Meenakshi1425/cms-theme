class CreateCustomProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :custom_products do |t|
      t.string :sort_field
      t.string :sort_direct
      t.integer :no_of_products

      t.timestamps
    end
  end
end
