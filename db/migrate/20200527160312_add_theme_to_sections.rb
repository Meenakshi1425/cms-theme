class AddThemeToSections < ActiveRecord::Migration[5.2]
  def change
    add_reference :sections, :theme, foreign_key: true
  end
end
