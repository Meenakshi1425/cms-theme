class HeaderController < ApplicationController
  def edit
    @cms = Theme.find(params[:cm_id])
    @header = Header.find(params[:id])
    @header.menus.build
  end

  def update
    @header = Header.find(params[:id])
    @cms = Theme.find(params[:cm_id])
    @widget = Theme.find(params[:cm_id])
    if @header.update_attributes(set_header_params)
      redirect_to cm_widget_path(@cms, @widget)
    else
      render :edit
    end
  end

  private
  def set_header_params
    params.require(:header).permit(:logo, menus_attributes: Menu.attribute_names.map(&:to_sym).push(:_destroy))
  end
end
