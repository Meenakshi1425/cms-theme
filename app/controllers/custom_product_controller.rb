class CustomProductController < ApplicationController
    def edit
        @custom_product = CustomProduct.last
    end

    def update
        @cms = Theme.find(params[:cm_id])
        @widget = Theme.find(params[:cm_id])
        @custom_product = CustomProduct.last
        params[:custom_product][:no_of_products] =  params[:custom_product][:no_of_products].to_i if params[:custom_product][:no_of_products].present?
        if @custom_product.update_attributes(set_product)
            redirect_to cm_widget_path(@cms, @widget)
        else
            render :edit
        end
    end

    private
    def set_product
        params.require(:custom_product).permit(:sort_field, :sort_direct, :no_of_products)
    end
end
