class WidgetController < ApplicationController
  def show
    @widget = Widget.find(params[:id])
    @cms = Theme.find(params[:id])
    @header = Header.find(@widget.header_id)
    @menus = @header.menus
    @logo = @header.logo
    @banner = Banner.last
    @custom_product = CustomProduct.last
    @renderer = ERB.new(@widget.body)
    @hbody = ERB.new(@widget.hbody)
    @detail = ERB.new(@widget.detail)
    @products = Spree::Product.order("#{@custom_product[:sort_field]} #{@custom_product[:sort_direct]}").limit(@custom_product[:no_of_products])
  end

  def update
    Widget.all.each do |w|
      if w.id == params[:id].to_i
        w.published = true
      else
        w.published = false
      end
      w.save
    end
    redirect_to spree_path
  end
end
