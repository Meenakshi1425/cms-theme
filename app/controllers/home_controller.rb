class HomeController < ApplicationController
  def index
    @themes = Theme.all
  end

  def show
    @theme = Theme.find(params[:id])
    @header = Header.find(@theme.header_id)
    @menus = @header.menus
    @logo = @header.logo
    @renderer = ERB.new(@theme.body)
    @hbody = ERB.new(@theme.hbody)
    @detail = ERB.new(@theme.detail)
  end

  def change_site
    Widget.all.each do |w|
      w.published = false
      w.save
    end
    redirect_to spree_path
  end
end
