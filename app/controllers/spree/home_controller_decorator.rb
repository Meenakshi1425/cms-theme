module Spree
  Spree::HomeController.class_eval do

    def index
      if @widget.present?
        @custom_product = CustomProduct.last
        @products = Spree::Product.order("#{@custom_product[:sort_field]} #{@custom_product[:sort_direct]}").limit(@custom_product[:no_of_products])
      else
        @searcher = build_searcher(params.merge(include_images: true))
        @products = @searcher.retrieve_products
        @products = @products.includes(:possible_promotions) if @products.respond_to?(:includes)
        @taxonomies = Spree::Taxonomy.includes(root: :children)
      end
    end

  end
end