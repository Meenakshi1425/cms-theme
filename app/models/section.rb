class Section < ApplicationRecord
    belongs_to :theme
    belongs_to :widget
end
