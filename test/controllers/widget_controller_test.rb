require 'test_helper'

class WidgetControllerTest < ActionDispatch::IntegrationTest
  test "should get show" do
    get widget_show_url
    assert_response :success
  end

end
